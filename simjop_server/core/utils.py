import os


def fix_path(path):
    return os.path.abspath(os.path.expanduser(path))


def mkdir_if_not_exists(path):
    abs_path = fix_path(path)
    if not os.path.exists(abs_path):
        os.mkdir(abs_path)
