#!/bin/bash

VIRTUAL_ENV=/opt/venv
python3 -m venv $VIRTUAL_ENV
PATH="$VIRTUAL_ENV/bin:$PATH"

python simjop_server run
